
#include "Arduino.h"
#include <FastLED.h>
#include <Servo.h>
#include <math.h>
#include "main.hpp"
#include "Timer.hpp"
#include "AvgReading.hpp"

// #define DEBUG 1

#define CIRCLE_PIN 4
#define CIRCLE_LENGTH 24 + 1
#define STRIP_PIN 5
#define STRIP_LENGTH 8 + 1
#define POT_PIN 7
#define COLORS_COUNT 7
#define READING_HISTORY_LENGTH 10

CRGB circle_leds[CIRCLE_LENGTH];
CRGB strip_leds[STRIP_LENGTH];

Timer ledTimer(200);
Timer servoTimer(50);

Servo myservo;

int servoAngle = 0;

CRGB colorSet[COLORS_COUNT] = {CRGB::Red, CRGB::OrangeRed, CRGB::Yellow, CRGB::Green, CRGB::LightSkyBlue, CRGB::Blue, CRGB::Violet};
uint8_t circleColorIndex;
uint8_t circleIndex;
uint8_t stripColorIndex;
uint8_t stripIndex;

uint16_t avgReading = 0;
AvgReading reading(0);

void setup()
{
  Serial.begin(9600);

  myservo.attach(9); // attaches the servo on pin 9 to the servo object

  FastLED.addLeds<NEOPIXEL, CIRCLE_PIN>(circle_leds, CIRCLE_LENGTH);
  FastLED.addLeds<NEOPIXEL, STRIP_PIN>(strip_leds, STRIP_LENGTH);

  FastLED.setBrightness(10);
  resetCircle();
  resetStrip();

  ledTimer.resetTimer();
  servoTimer.resetTimer();

  reading.resetReadings(analogRead(POT_PIN));
}

void loop()
{

  if (servoTimer.timerExpired())
  {
    // read potentiometer
    reading.addReading(analogRead(POT_PIN));
    avgReading = reading.getAvgReading();

    /*
	  val = (reading/1024.0) * STRIP_LENGTH;
    colorVal = (reading/1024.0) * 255;
    */
    servoAngle = map(avgReading, 0, 1023, 0, 180); // scale it to use it with the servo (value between 0 and 180)
#ifdef DEBUG
    Serial.println("=-=-=-=-=-=");
    Serial.println(avgReading);
    Serial.println(servoAngle);
#endif
    myservo.write(servoAngle);
    servoTimer.resetTimer();
  }

  if (ledTimer.timerExpired())
  {
    handleCircle();
    handleStrip();
    ledTimer.resetTimer();
  }

} // loop

void resetCircle()
{
  circleColorIndex = 0;
  circleIndex = 0;
}
void handleCircle()
{
  if (circleIndex == CIRCLE_LENGTH)
  {
    circleIndex = 0;
    if (++circleColorIndex == COLORS_COUNT)
    {
      circleColorIndex = 0;
    }
  }
  circle_leds[circleIndex] = colorSet[circleColorIndex];
  FastLED.show();
  circleIndex++;
}

void resetStrip()
{
  stripColorIndex = 0;
  stripIndex = 0;
}
void handleStrip()
{
  if (stripIndex == STRIP_LENGTH)
  {
    stripIndex = 0;
    if (++stripColorIndex == COLORS_COUNT)
    {
      stripColorIndex = 0;
    }
  }
  strip_leds[stripIndex] = colorSet[stripColorIndex];
  FastLED.show();
  stripIndex++;
}
