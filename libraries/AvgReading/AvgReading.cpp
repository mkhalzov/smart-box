#include "AvgReading.hpp"

AvgReading::AvgReading(uint16_t initialValue = 0)
{
    this->resetReadings(initialValue);
}

void AvgReading::resetReadings(uint16_t initialValue = 0)
{
    // avgReading = 0;
    for (uint8_t i = 0; i < READING_HISTORY_LENGTH; i++)
    {
        readingHistory[i] = initialValue;
    }
    currentReadingIndex = 0;
}

void AvgReading::addReading(uint16_t reading) {
    if (++currentReadingIndex == READING_HISTORY_LENGTH)
    {
        currentReadingIndex = 0;
    }
    readingHistory[currentReadingIndex] = reading;
}

uint16_t AvgReading::getAvgReading() {
    uint16_t sum = 0, avgReading = 0;
    for (uint8_t i = 0; i < READING_HISTORY_LENGTH; i++)
    {
        sum += readingHistory[i];
    }
    avgReading = sum / READING_HISTORY_LENGTH;
    return avgReading;
}

    