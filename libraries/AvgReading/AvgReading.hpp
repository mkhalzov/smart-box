#ifndef AVG_READING
#define AVG_READING

#include <Arduino.h>

#define READING_HISTORY_LENGTH 10

class AvgReading
{
public:
    AvgReading(uint16_t);
    void resetReadings(uint16_t);
    void addReading(uint16_t);
    uint16_t getAvgReading();

private:
    uint16_t readingHistory[READING_HISTORY_LENGTH];
    uint8_t currentReadingIndex;
};

#endif